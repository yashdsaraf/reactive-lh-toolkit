package org.openmrs.repository;

import org.openmrs.model.CObservation;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface ObservationRepository extends ReactiveCrudRepository<CObservation, String> {

}
